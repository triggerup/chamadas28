import React, { useEffect, useState } from "react";
import { Image, AppState, View, Text, Button } from "react-native";
import { Video } from "expo-av";
import { fit, link, mediaDir } from './uteis';
import { Logo, sicronizeMedia } from './uteis';
import * as Permissions from 'expo-permissions';

let refreshPubsTime = null;
let checking = false;
let initiated
let conectionNow = 0;
export default (props) => {
    // const [permission, askForPermission] = Permissions.usePermissions(Permissions.CAMERA,Permissions.CAMERA_ROLL);


    const width = fit(1980);
    const height = fit(1080);
    const logoImage = <Logo height={height} width={width} />;

    if (props.socket === null)
        return (<Logo width={width} height={height} />);

    const [index, setIndex] = useState(0);
    const [mediaOk, setMediaOk] = useState(false);
    const [publicidades, setPublicidades] = useState([]);
    const [isChamadaServer, setIsChamadaServer] = useState(props.isServer);


    useEffect(() => {
        setIsChamadaServer(props.isServer);
    }, [props.isServer, props.isConected]);

    const [emited, setEmited] = useState(false);

    // const refreshPubs = () => {
    //     clearInterval(refreshPubsTime);
    //     refreshPubsTime = setInterval(() => {
    //         fetch(link + "/api/marketinginterno/getPubs").then(result => result.json().then(data => {
    //             setPublicidades(data);
    //         })).catch(err => console.err("Falha ao buscar pubs:", err));
    //     }, 15000);
    // }

    let emitChangeVideo = (index) => {
        props.socket.emit("changeVideo", index);
    };

    const nextSlide = (videoId) => {
        if (isChamadaServer) {
            if (index < publicidades.length - 1) {
                const next = index + 1;
                emitChangeVideo(next);
                setIndex(next);
            } else { setIndex(0); emitChangeVideo(0) };
        }
        if (videoId > -1) {
            setIndex(videoId);
        }
    };

    useEffect(() => {
        AppState.addEventListener("change", (status) => {
            if (status === "active") {
                props.socket.connect();
            } else {
                props.socket.disconnect();
            }
        });
        props.socket.on("playNextVideo", (index) => {
            setEmited(true);
            nextSlide(index);
        });
        props.socket.on("setList", data => {
            setPublicidades(data);
        });

        const socke = props.socket;
        initiated = setInterval(() => {
            console.log("Checagem")
            if (!conectionNow) {
                socke.connect();
                console.log("Tentando conectar: ")
            }
        }, 5000);

        return () => {
            clearInterval(initiated),
                console.log("Limpando Verificador")
        }

    }, []);

    useEffect(() => {
        conectionNow = props.isConected;
    }, [props.isConected])

    useEffect(() => {

        setTimeout(() => {
            if (publicidades.length > 0 && mediaOk === false && !checking) {
                checking = true;
                sicronizeMedia(publicidades, (result, status) => {
                    props.onProgress(result); setMediaOk(status); checking = false;
                }).catch(() => { setPublicidades(publicidades); checking = false });
            } else {
                // refreshPubs();
                props.onProgress(mediaOk ? "" : "⚠️");
            }
            // return () => {clearInterval(refreshPubsTime) };
        }, 1000);

    }, [publicidades, mediaOk]);



    try {
        if (publicidades.length < 1) {
            return logoImage;
        }

        const media = publicidades[index];
        const split = media.source.split("/");
        const fileName = split[split.length - 1];

        if ((emited || isChamadaServer) && mediaOk) {


            let src = { uri: mediaDir + fileName };

            if (media.tipo === "video" || media.tipo === "vid") {
                return (
                    <VideoSlide source={src} width={width} height={height} vol={props.vol} onDidFinish={() => nextSlide()} />
                );
            } else {
                if (media.tipo === "img" || media.tipo === "image")
                    return (<ImageSlide source={src} time={15} onDidFinish={() => nextSlide()} width={width} height={height} />);
                else
                    return logoImage;
            }
        } else {
            return logoImage;
        }
    } catch (error) {
        console.error(error);
        return logoImage;
    }
};

const ImageSlide = (props) => {

    useEffect(() => {
        setTimeout(() => props.onDidFinish(), props.time * 1000)
    }, []);


    return (<Image source={props.source} style={{ flex: 0.8, resizeMode: "contain", width: props.width, height: props.height, marginBottom: 1, }} />);
}
const VideoSlide = (props) => {

    return (<Video source={props.source} rate={1.0} volume={props.vol ? props.vol : 0.18} isMuted={false} resizeMode={"contain"} shouldPlay isLooping={false}
        onPlaybackStatusUpdate={(ev) => {
            if ((ev.didJustFinish || ev.error)) {

                props.onDidFinish();

            }
        }}
        style={{ width: props.width, height: props.height, flex: 1, margin: 1, }}
    />);
}