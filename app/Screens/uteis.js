import React from 'react';
import { Dimensions, Image } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Speech from "expo-speech";
import imgLogo from "../assets/logo800.png";
import * as Fs from "expo-file-system";


//const uri = __DEV__ ? "http://192.168.10.105:21253" : "http://server.clinica28dejulho.com.br:21253";
const uri = "http://server.clinica28dejulho.com.br:21253";
//const uri = "http://192.168.254.28:21253";

export const link = uri;

export const storeSetItem = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.error(error);
  }
};


export const storeGetItem = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
  } catch (error) {
    return null;
  }
};

export const fit = (size) => {
  return size * (Math.round(Dimensions.get("window").width) / 640);
};

export const speak = (frase, options, isOneTime) => {
  try {
    if (!isOneTime)
      Speech.speak(frase, { onDone: () => { Speech.speak(frase, options) } });
    else
      Speech.speak(frase, options);
  } catch (e) {
    console.info(e);
    try { () => { options.onDone() } } catch (erro) { }
  }
}
export const Logo = (props) => (
  <Image
    source={imgLogo}
    style={{
      flex: 0.8,
      resizeMode: "contain",
      width: props.width,
      height: props.height,
      marginBottom: 1,
    }}
  />
);
const urlSource = "http://api.clinica28dejulho.com.br/";
export const mediaDir = Fs.documentDirectory + "media/";
export const sicronizeMedia = async (publicidades, handleProgress) => {
  try {

    const md = await Fs.getInfoAsync(mediaDir);
    if (!md.exists) {
      await Fs.makeDirectoryAsync(mediaDir);
    }
    for (let i = 0; i < publicidades.length; i++) {
      const publicidade = publicidades[i];
      const split = publicidade.source.split("/");
      const fileName = split[split.length - 1];
      const fileUri = mediaDir + fileName;
      const file = await Fs.getInfoAsync(fileUri);
      const progress = (i + 1) + "/" + publicidades.length;

      handleProgress(progress, false);

      if (await Fs.getFreeDiskStorageAsync() < 100000000) {
        handleProgress(progress + " low space", false);
      }
      if ((await Fs.getInfoAsync(fileUri + ".part")).exists) {
        await Fs.deleteAsync(fileUri + ".part");
        console.log("Apagando .part");
      }
      let andamento = null;
      if (!file.exists) {
        let notDownload = true

        while (notDownload) {
          try {
            notDownload = false;
            const download = Fs.createDownloadResumable(
              publicidade.source,
              fileUri + ".part",
              { cache: false, sessionType: Fs.FileSystemSessionType.FOREGROUND },
              (ev) => {
                const baixado = ev.totalBytesWritten;
                const total = ev.totalBytesExpectedToWrite;
                const porcento = (((baixado / total)) * 100).toFixed(1) + "%";
                console.log("progress ", progress + " " + porcento);
                handleProgress(progress + " " + porcento, false);
                if (baixado === total) {
                  andamento = Fs.copyAsync({ from: fileUri + ".part", to: fileUri });
                }
              });
            console.log("iniciar download de arquivo que falta")
            let result = await download.downloadAsync().catch((err) => {
              notDownload = true;
              console.log(err)
            });
            console.log("result", result);
          } catch (error) {
            notDownload = true;
          }
        }
      } else {
        console.log("Arquivo conferido.");
      }
      if (andamento !== null) {
        console.log("aguardando andamento");
        await andamento;
        console.log("andamento concluido");
      }
    }
    let listPublicidades = [];
    //Checagem final
    let checkFinal = true;
    for (let i = 0; i < publicidades.length; i++) {
      const pub = publicidades[i];
      const splt = pub.source.split("/");
      const imgName = splt[splt.length - 1];
      listPublicidades.push(pub.source);
      const file = await Fs.getInfoAsync(mediaDir + imgName);
      if (!file.exists) {
        checkFinal = false;
      }
    }
    //Limpeza de arquivos inulteis
    /*let dir = await Fs.getInfoAsync(mediaDir);
    if (dir.exists) {
      const dirList = await Fs.readDirectoryAsync(mediaDir);
      console.log("dirlist", dirList);
      for (let i = 0; i < dirList.length; i++) {
        const img = "imagens/" + dirList[i];
        let found = listaPub.find((value) => {
          if (value === img) return true;
        });
  
        if (!found) {
          await Fs.deleteAsync(mediaDir + img);
        }
      }
    }*/
    console.log("Checagem final media =" + checkFinal);
    handleProgress("", checkFinal);
    return checkFinal; 
  } catch (error) {
    return false
  }
};