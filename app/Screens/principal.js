import React, { useState, useEffect } from "react";
import { activateKeepAwake } from "expo-keep-awake";
import { Container, CardItem, Text, Card, Icon } from "native-base";
import { View, Image, FlatList } from "react-native";
import Relogio from "./relogio";
import socketIO from "socket.io-client";
import { expo } from "./../app.json";
import { fit, speak, storeSetItem, link } from './uteis';
import LogoWhite from "../assets/logoWhite2.png";
import Canal from './canal';

const pacienteTeste = {
  id: new Date().getMilliseconds(),
  paciente: "Fulano Teste Teste",
  consultorio: "0",
  andar: "térreo",
};


let atualVol = 0; 
//let DataAgora =new Date('07/26/21').toLocaleDateString();
let DataAgora =new Date().toLocaleDateString();
export default (props) => {

  const uri = link;
  let version = expo.version;
  const vol1 = 0.18, vol2 = 0.40, vol3 = 0.78;

  const [chamados, setChamados] = useState([]);
  const [unidadeN, setUnidadeN] = useState(props.unidade);
  const [connection, setConnection] = useState(0);
  const [chamando, setChamando] = useState(false);
  const [isServer, setIsServer] = useState(false);
  const [paciente, setPaciente] = useState(null);
  const [socket, setSocket] = useState(null);
  const [progress, setProgress] = useState("");
  const [vol, setVol] = useState(props.vol); 
  
  const changeDataAtual =()=>{  
    
    if(DataAgora !== new Date().toLocaleDateString()){ 
      console.log("agora !!",DataAgora); 
      DataAgora = new Date().toLocaleDateString(); 
      let list = [];  
      setChamando(false);
      setPaciente(null);
      setChamados(list);
      setVol(atualVol); 
      console.log("depois!!",DataAgora);  
      console.log("chamados:",chamados)  
    }
  }

  const chamar = (item) => {
    setPaciente(item);
    setVol(vol1);
    if (!chamando) {
      //setTimeout(() =>setChamando(true),1000);
      setChamando(true);
    }
  }
  const handleDoneChamar = () => {
    const item = paciente;
    let list = chamados.slice();
    item.hora = new Date().toTimeString().substring(0, 5);
    list.push(item);
    setChamando(false);
    setPaciente(null);
    setChamados(list);
    setVol(atualVol);
  }

  useEffect(() => {
    atualVol = props.vol;
    activateKeepAwake();
    const socketSetup = socketIO(uri, {
      transportes: ["websocket"],
      query: { unidade: unidadeN, app: 'chamadas28' },
    });

    socketSetup.on("chamadas28/call", paciente => {
      chamar(paciente);
    });
    socketSetup.on("chamadas28/aviso", texto => {
      if (!chamando) {
        setVol(vol1);
        speak(texto, { onDone: () => { setVol(atualVol) } }, true);
      }
    });
    socketSetup.on("connect", () => {
      setConnection(1);
    });
    socketSetup.on("disconnect", () => {
      setIsServer(false);
      setConnection(0);
    });

    socketSetup.on("setCliente", () => {
      setIsServer(false);
    });

    socketSetup.on("setChamadaServer", () => {
      setIsServer(true);
    });
    socketSetup.on("connect_error", () => {
      console.log("connect")
      socketSetup.connect()
    });

    console.log("erro saiu");

    setSocket(socketSetup);
    return (() => { socketSetup.disconnect() })
  }, []);
  function chamarTeste() {
    chamar(pacienteTeste);
  }

  function changeVol() {
    atualVol = vol >= vol1 ? (vol >= vol2 ? (vol === vol3 ? vol1 : vol3) : vol2) : vol1;
    setVol(atualVol);
    storeSetItem("vol", atualVol.toString());
  }

  function changeUnidade() {
    let un = unidadeN == 1 ? 2 : 1;
    socket.emit("changeUnidade", un);
    setUnidadeN(un);
    storeSetItem("unidade", un.toString());
  }
  console.log("antes", connection)
  return (
    <>
      <Container>
        <View style={{ flex: 0.12, flexDirection: "row", backgroundColor: "darkblue", paddingTop: 5, paddingBottom: 5, }}>
          <View style={{ flex: 0.01 }}></View>
          <View style={{ flexDirection: "column", flex: 0.15, backgroundColor: "darkblue", justifyContent: "center", padding: 10, }} >
            <Image source={LogoWhite} style={{ flex: 0.9, resizeMode: "contain", width: 100, height: 100, }} />
          </View>
          <View style={{ flexDirection: "column", flex: 0.8, justifyContent: "center", backgroundColor: "darkblue", padding: 10, }}>
            <DisplayPaciente paciente={paciente} chamar={chamando} onDone={handleDoneChamar} />
          </View>
          <View style={{ flex: 0.01 }}></View>
        </View>
        <View style={{ flex: 0.9, flexDirection: "row" }}>
          <View style={{ flex: 0.03 }}></View>
          <View style={{ flex: 0.8, padding: 1, flexDirection: "column", alignItems: "center", justifyContent: "center", }} >

            {/*this.state.connectionfalse ? (
              <PubPanel publicidades={this.state.publicidades} width={fit(1920)} height={fit(1080)} setPlayNextVideo={this.setPlayNextVideo} socket={this.socket} isChamadaServer={this.state.isChamadaServer} setDownloadedPub={(n) => this.setDownloadedPub(n)} />
            ) : null*/}
            <Canal socket={socket} isConected={connection} isServer={isServer} onProgress={(progress) => { setProgress(progress) }} vol={vol} />
          </View>
          <View style={{ flex: 0.03 }}></View>
          <View style={{ flex: 0.2, flexDirection: "column", margin: 2, marginHorizontal: 4, paddingBottom: 0, }}>
            <View style={{ flex: 0.1, backgroundColor: "orange", alignItems: "center", justifyContent: "center", }} >
              <Text style={{ fontWeight: "bold", fontSize: fit(10), color: "white", }}>
                Últimas Chamadas
              </Text>
            </View>
            <View style={{ flex: 0.9, padding: 3, marginBottom: 0 }}>
              <FlatList
                data={chamados.slice().reverse()}
                renderItem={(props) => chamadoItem(props)}
                keyExtractor={({ id }) => id.toString()}
              />
            </View>
          </View>
        </View>
        <View style={{ flex: 0.02 }}></View>
        <View style={{ flex: 0.04, backgroundColor: "darkblue", justifyContent: "center", flexDirection: "row", paddingBottom: 5, paddingLeft: 5, paddingRight: 5, }} >
          <View style={{ flex: 0.03 }}></View>
          <View style={{ flexDirection: "column", flex: 0.4, justifyContent: "center", alignItems: "flex-start", paddingLeft: 10, }} >
            <Text onPress={changeUnidade} style={{ fontSize: fit(8), color: "white", fontWeight: "bold", }} >
              Unidade:{unidadeN}
            </Text>
          </View>
          <View style={{ flexDirection: "column", flex: 0.4, justifyContent: "center", alignItems: "center", }} >

            <Text onPress={() => { connection ? socket.disconnect() : socket.connect() }} style={{ color: "white", fontWeight: "bold", fontSize: fit(8), }} >
              <Relogio onChengeTime={()=>changeDataAtual()} dataAgora={DataAgora} />
            </Text>

          </View>
          <View style={{ flexDirection: "column", flex: 0.4, justifyContent: "center", alignItems: "flex-end", }} >
            <Text onPress={changeVol} style={{ color: "white", fontWeight: "bold", fontSize: fit(8), marginRight: 3, }} >
              {isServer ? " ♾️" : ""} {progress + "   "}
              {version}
              {"  " + vol <= vol1 ? "🔈" : vol <= vol2 ? "🔉" : vol <= vol3 ? "🔊" : "🔇"}
            </Text>
          </View>
          <View onPress={chamarTeste} style={{ flexDirection: "column", flex: 0.05, justifyContent: "center", alignItems: "flex-start", paddingRight: 10, }} >
            <Icon name="desktop" style={{ color: connection ? "green" : "red", fontWeight: "bold", fontSize: fit(8), paddingLeft: 8, }} />
          </View>
          <View style={{ flex: 0.03 }}></View>
        </View>
      </Container>
    </>);
};




const chamadoItem = ({ item, index }) => {
  return (
    <Card>
      <CardItem style={{ paddingLeft: 3, paddingRight: 3, paddingBottom: 3, paddingTop: 3, }} >
        <Text style={{ alignContent: "center" }}>
          <Text style={{ fontWeight: "bold", fontSize: fit(9), textAlign: "justify", width: 10 }} numberOfLines={1} ellipsizeMode={"tail"} >
            {item.paciente}
          </Text>
          {"\n"}
          <Text style={{ fontSize: fit(7), textAlign: "center" }}>
            Consultório {item.consultorio} - {item.andar} - Hora:{item.hora}
          </Text>
        </Text>
      </CardItem>
    </Card>);
}



const DisplayPaciente = (props) => {
  try {

    const [busy, setBusy] = useState(false);
    const item = props.paciente;

    if (props.chamar) {
      const fala = item.paciente + ". Por favor! Dirija-se ao consultório " + item.consultorio + " no " + item.andar + ".";
      if (!busy) {
        try {
          speak(fala, {
            onDone: () => {
              props.onDone();
              setBusy(false);
            }, rate: 1.0
          });
          setBusy(true);
          return null;

        } catch (error) {
          console.error("Não foi possível chamar:", error);
          props.onDone();
          return null;
        }
      }

      return (<>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{ color: "white", textAlignVertical: "bottom", fontSize: fit(20), marginLeft: 2 }}>
            {item.paciente}
          </Text>
          <Text style={{ flex: 1, color: "white", textAlignVertical: "bottom", fontSize: fit(11), marginLeft: fit(10), }}>
            Consultório {item.consultorio} - {item.andar}
          </Text>
        </View>
      </>);
    } else {
      return null;
    }

  } catch (error) {
    return null
  }
};
