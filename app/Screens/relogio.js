import React, { useState, useEffect } from 'react';
import { Text } from 'native-base';
import { fit } from './uteis';

let relogio = null;
export default (props) => {

    const [timeLocal, setTimeLocal] = useState(new Date().toLocaleTimeString());
    relogio = setInterval(() => { setTimeLocal(new Date().toLocaleTimeString()) }, 1000);

    useEffect(()=>{ 
            props.onChengeTime() 
    })

    useEffect(() => {
        setTimeLocal(new Date().toLocaleTimeString());
        return () => { clearInterval(relogio) }
    }, [relogio]);


    return (<>
        <Text onPress={() => {} } style={{ color: "white", fontWeight: "bold", fontSize: fit(8), }} >
            🕐 {timeLocal}
        </Text>
    </>);
}
