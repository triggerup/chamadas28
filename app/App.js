import React, {useState} from 'react';
import { AppLoading } from 'expo';
import {StatusBar} from 'react-native';

import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import Principal from './Screens/principal'
import { storeGetItem } from './Screens/uteis';
//import Main from './Screens/teste';
//import { ScreenOrientation } from 'expo';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  //  ScreenOrientation.lockAsync(ScreenOrientation.Orientation.LANDSCAPE);
    this.state = {
      isReady: false,
    };
  }

  async componentDidMount() {
    StatusBar.setHidden(true);
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({ isReady: true });
  }

  render() {
    if (!this.state.isReady) {
      return <AppLoading />;
    }
    return (
      <LoadPrincipal />
    );
  }
}

  const  LoadPrincipal = (props) => {
  const [unidadeLoaded, setLoaded] = useState(false);
  const [vol, setVol] = useState(0.40);
  storeGetItem("vol").then((result) => {
    const gotVol = result === undefined ? 0.40 : parseFloat(result);
    setVol(gotVol);
  });
  storeGetItem("unidade").then((result) => {
    const unidade = result === undefined ? 1 : result;
    setLoaded(unidade);
  });
  return (unidadeLoaded ? <Principal unidade={unidadeLoaded} vol={vol} /> : <></>);
}